﻿using System;
using System.IO;
using Unit4.CollectionsLib;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX19_IlayShtechman
{
    public partial class Calendar : Form
    {
        public static bool showing = false;
        public Calendar()
        {
            InitializeComponent();
        }

        private void CelebSubmit_Click(object sender, EventArgs e)
        {
            string date = CelebDate.Text;
            string name = CelebName.Text;
            string data = "";
            if (LoginService.logged)
            {
                if(File.Exists(Directory.GetCurrentDirectory() + "\\" + LoginService.loggedName+ "BD.txt"))
                {
                    data = File.ReadAllText(Directory.GetCurrentDirectory() + LoginService.loggedName + "BD.txt");
                }
            }
            else
            {
                MessageBox.Show("You aren't logged yet, you can't save birthdays");
                this.Dispose();
                return;
            }
            if(data == "")
            {
                data = name + "," + date;
            }
            else
            {
                data += Environment.NewLine + name + "," + date;
            }

            File.WriteAllText(Directory.GetCurrentDirectory() + "\\" + LoginService.loggedName + "BD.txt", data);
        }

        public static int QueueAmount(Queue<DateTime> dt)
        {
            int amount = 0;
            Queue<DateTime> tmp = new Queue<DateTime>();
            while(!dt.IsEmpty())
            {
                amount++;
                tmp.Insert(dt.Remove());
            }
            while(!tmp.IsEmpty())
            {
                dt.Insert(tmp.Remove());
            }
            return amount;
        }
        public static void QueueInArray(Queue<DateTime> dt,ref DateTime[] arr)
        {
            int amount = QueueAmount(dt);
            for (int i = 0; i < amount; i++)
            {
                arr[i] = dt.Remove();
            }
        }

        private void UpdButton_Click(object sender, EventArgs e)
        {
            int datesAmount = 0;
            int month = 0, day = 0, year = 0;
            Queue<DateTime> dates = new Queue<DateTime>();
            DateTime[] inCal;
            string tmpDate = "";
            string[] EditedDate = new string[3];
            string tmpName = "";
            string[] name_date = new string[2];
            string[] data;
            if (LoginService.logged)
            {
                if(File.Exists(Directory.GetCurrentDirectory()+ LoginService.loggedName + "BD.txt"))
                {
                    data = File.ReadAllLines(Directory.GetCurrentDirectory() + LoginService.loggedName + "BD.txt");
                }
                else
                {
                    data = new string[50];
                }
            }
            else
            {
                MessageBox.Show("You aren't logged yet, you can't save birthdays");
                this.Dispose();
                return;
            }

            foreach (string d in data)
            {
                name_date = d.Split(',');
                tmpName = name_date[0];
                tmpDate = name_date[1];
                EditedDate = tmpDate.Split('/');
                day = int.Parse(EditedDate[0]);
                month = int.Parse(EditedDate[1]);
                year = int.Parse(EditedDate[2]);
                dates.Insert(new DateTime(year,month, day, 0, 0, 0));
            }
            datesAmount = QueueAmount(dates);
            inCal = new DateTime[datesAmount];
            QueueInArray(dates, ref inCal);
            monthCale.AnnuallyBoldedDates = inCal;

        }

        private void CelebDate_ValueChanged(object sender, EventArgs e)
        {
            monthCale.SetDate(CelebDate.Value);
        }

        private void monthCale_DateChanged(object sender, DateRangeEventArgs e)
        {
            CelebDate.Value = monthCale.SelectionStart;

        }
    }
}
