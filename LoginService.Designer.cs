﻿namespace EX19_IlayShtechman
{
    partial class LoginService
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.User_Textbox = new System.Windows.Forms.TextBox();
            this.Pass_TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LogButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // User_Textbox
            // 
            this.User_Textbox.Location = new System.Drawing.Point(79, 40);
            this.User_Textbox.Name = "User_Textbox";
            this.User_Textbox.Size = new System.Drawing.Size(193, 20);
            this.User_Textbox.TabIndex = 0;
            // 
            // Pass_TextBox
            // 
            this.Pass_TextBox.Location = new System.Drawing.Point(79, 79);
            this.Pass_TextBox.Name = "Pass_TextBox";
            this.Pass_TextBox.PasswordChar = '*';
            this.Pass_TextBox.Size = new System.Drawing.Size(193, 20);
            this.Pass_TextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Username :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password :";
            // 
            // LogButton
            // 
            this.LogButton.Location = new System.Drawing.Point(95, 165);
            this.LogButton.Name = "LogButton";
            this.LogButton.Size = new System.Drawing.Size(92, 31);
            this.LogButton.TabIndex = 4;
            this.LogButton.Text = "Login !";
            this.LogButton.UseVisualStyleBackColor = true;
            this.LogButton.Click += new System.EventHandler(this.LogButton_Click);
            // 
            // LoginService
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.LogButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Pass_TextBox);
            this.Controls.Add(this.User_Textbox);
            this.Name = "LoginService";
            this.Text = "LoginService";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox User_Textbox;
        private System.Windows.Forms.TextBox Pass_TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button LogButton;
    }
}