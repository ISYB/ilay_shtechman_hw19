﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX19_IlayShtechman
{
    public partial class Form1 : Form
    {
        public static bool shows = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void CalenButton_Click(object sender, EventArgs e)
        {
            Calendar cal = new Calendar();
            if(!Calendar.showing)
            {
                cal.Show();
                Calendar.showing = true;
            }
            else
            {
                cal.Hide();
                Calendar.showing = false;
            }
        }

        private void LogButton_Click(object sender, EventArgs e)
        {
            LoginService lg = new LoginService();
            if(LoginService.logged)
            {
                return;
            }
        }
    }
}
