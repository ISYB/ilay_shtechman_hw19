﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace EX19_IlayShtechman
{
    public partial class LoginService : Form
    {
        public static bool logged = false;
        public static string loggedName = "";
        public LoginService()
        {
            InitializeComponent();
            this.Show();
        }

        private void LogButton_Click(object sender, EventArgs e)
        {
            string[] lines = File.ReadAllLines("C:\\Users\\magshimim\\source\\repos\\EX19_IlayShtechman\\EX19_IlayShtechman\\Users.txt");
            string[] user_pass = new string[2];
            bool isCorrect = false;
            foreach (string line in lines)
            {
                user_pass = line.Split(',');
                if (user_pass[0] == User_Textbox.Text && user_pass[1] == Pass_TextBox.Text)
                {
                    isCorrect = true;
                }
            }
            if(isCorrect && !logged)
            {
                MessageBox.Show("Logged in succesfully !");
                logged = true;
                loggedName = User_Textbox.Text;
                this.Dispose();
            }
            else
            {
                if(logged)
                {
                    MessageBox.Show("you're logged in already!");
                }
                else
                {
                    MessageBox.Show("Check your credentials!");
                }
            }
        }

    }
}
