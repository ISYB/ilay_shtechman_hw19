﻿namespace EX19_IlayShtechman
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogButton = new System.Windows.Forms.Button();
            this.CalenButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LogButton
            // 
            this.LogButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LogButton.Location = new System.Drawing.Point(195, 263);
            this.LogButton.Name = "LogButton";
            this.LogButton.Size = new System.Drawing.Size(200, 83);
            this.LogButton.TabIndex = 0;
            this.LogButton.Text = "Login";
            this.LogButton.UseVisualStyleBackColor = true;
            this.LogButton.Click += new System.EventHandler(this.LogButton_Click);
            // 
            // CalenButton
            // 
            this.CalenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalenButton.Location = new System.Drawing.Point(-1, 263);
            this.CalenButton.Name = "CalenButton";
            this.CalenButton.Size = new System.Drawing.Size(200, 83);
            this.CalenButton.TabIndex = 1;
            this.CalenButton.Text = "Calendar";
            this.CalenButton.UseVisualStyleBackColor = true;
            this.CalenButton.Click += new System.EventHandler(this.CalenButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 341);
            this.Controls.Add(this.CalenButton);
            this.Controls.Add(this.LogButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button LogButton;
        private System.Windows.Forms.Button CalenButton;
    }
}

