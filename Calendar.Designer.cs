﻿namespace EX19_IlayShtechman
{
    partial class Calendar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.monthCale = new System.Windows.Forms.MonthCalendar();
            this.label1 = new System.Windows.Forms.Label();
            this.CelebDate = new System.Windows.Forms.DateTimePicker();
            this.CelebName = new System.Windows.Forms.TextBox();
            this.CelebSubmit = new System.Windows.Forms.Button();
            this.UpdButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // monthCale
            // 
            this.monthCale.Location = new System.Drawing.Point(0, 0);
            this.monthCale.MaxDate = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            this.monthCale.MinDate = new System.DateTime(1894, 1, 1, 0, 0, 0, 0);
            this.monthCale.Name = "monthCale";
            this.monthCale.TabIndex = 0;
            this.monthCale.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCale_DateChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(239, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Celebrator name : ";
            // 
            // CelebDate
            // 
            this.CelebDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.CelebDate.Location = new System.Drawing.Point(239, 67);
            this.CelebDate.MaxDate = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            this.CelebDate.MinDate = new System.DateTime(1894, 1, 1, 0, 0, 0, 0);
            this.CelebDate.Name = "CelebDate";
            this.CelebDate.Size = new System.Drawing.Size(200, 20);
            this.CelebDate.TabIndex = 2;
            this.CelebDate.ValueChanged += new System.EventHandler(this.CelebDate_ValueChanged);
            // 
            // CelebName
            // 
            this.CelebName.Location = new System.Drawing.Point(242, 32);
            this.CelebName.Name = "CelebName";
            this.CelebName.Size = new System.Drawing.Size(196, 20);
            this.CelebName.TabIndex = 3;
            // 
            // CelebSubmit
            // 
            this.CelebSubmit.Location = new System.Drawing.Point(281, 108);
            this.CelebSubmit.Name = "CelebSubmit";
            this.CelebSubmit.Size = new System.Drawing.Size(137, 29);
            this.CelebSubmit.TabIndex = 4;
            this.CelebSubmit.Text = "Submit Birthday !";
            this.CelebSubmit.UseVisualStyleBackColor = true;
            this.CelebSubmit.Click += new System.EventHandler(this.CelebSubmit_Click);
            // 
            // UpdButton
            // 
            this.UpdButton.Location = new System.Drawing.Point(282, 158);
            this.UpdButton.Name = "UpdButton";
            this.UpdButton.Size = new System.Drawing.Size(135, 32);
            this.UpdButton.TabIndex = 5;
            this.UpdButton.Text = "Update user\'s calendar";
            this.UpdButton.UseVisualStyleBackColor = true;
            this.UpdButton.Click += new System.EventHandler(this.UpdButton_Click);
            // 
            // Calendar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 258);
            this.Controls.Add(this.UpdButton);
            this.Controls.Add(this.CelebSubmit);
            this.Controls.Add(this.CelebName);
            this.Controls.Add(this.CelebDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.monthCale);
            this.Name = "Calendar";
            this.Text = "Calendar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar monthCale;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker CelebDate;
        private System.Windows.Forms.TextBox CelebName;
        private System.Windows.Forms.Button CelebSubmit;
        private System.Windows.Forms.Button UpdButton;
    }
}